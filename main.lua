-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

--require statements

--[[forward declarations]]--
local screenCenterX = display.contentCenterX
local screenCenterY = display.contentCenterY
local _W = display.contentWidth
local _H = display.contentHeight 

local nr = math.random

local var = 99
print( "Hello World!" )
print( var .. " bottles of beer on the wall")

local objects = { "enemy" , "friend" }
local alien
local score = 0
local scoreObj

--[[set up our display]]
local function setupDisplay()
	scoreObj = display.newText( "Score: " .. score, screenCenterX, 0, native.systemFont, 16 )

	--ground display
	local ground = display.newRect( screenCenterX, _H + 20 , _W, _H/8 )
	ground:setFillColor( 209/256, 169/256, 113/256 )

	--alien display
	alien = display.newImageRect( "img/alienGreen.png", 66/2, 92/2 )
	alien.x = screenCenterX
	alien.y = ground.y - ground.height + 10
	alien.type = "alien"
end


--[[set up basic interactivity]]
local function onTouch(event)
	if event.phase == "began" then
		local speed = 1500/_W + (math.abs(alien.x - event.x))
		transition.to(alien, {time = speed, x = event.x})
	end
end

--[[set up falling objects]]
local function spawnObject()
	local objIndex = nr(#objects)
	local objName = objects[objIndex]
	local object = display.newImage( "img/" .. objName .. ".png")
	object.x = nr(30, _W - 30)
	object.y = 0
	object.rotation = nr(-15, 15)
	object.type = objName
end


setupDisplay()
display.currentStage:addEventListener( "touch", onTouch )

